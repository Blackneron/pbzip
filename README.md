# PBzip Tool - README #
---

### Overview ###

The **PBzip** tool is one file which can be uploaded to a webhost together with a ZIP file. The ZIP file can contain something like a whole website, CMS or web application which should be installed on the host. The upload is a lot faster because there is only one ZIP file to upload which contains all the necessary installation files. After uploading the two files, the **PBzip** tool can unzip the archive on the host. After decompressing the files the **PBzip** tool should be renamed or deleted. This tool uses the [**PhpConcept Zip Module 2.8.2 of Vincent Blavet**](https://www.phpconcept.net/pclzip).

### Screenshot ###

![PBzip Tool](development/readme/pbzip.png "PBzip Tool")

### Versions ###

There are two versions of the **PBzip** tool:

#### Plain Text Version (283 KB) ####
The first version is the plain text version of the **PBzip** tool.


#### Minified Version (75 KB) ####
The second version is a minified version of the **PBzip** tool.

### Setup ###

* Just upload the file **pbzip.php** to your webhost, together with a ZIP file.
* Navigate with your browser to the **PBzip** tool.
* Decompress your ZIP file.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBzip** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
