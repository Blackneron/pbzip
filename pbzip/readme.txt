
Some important information about the PBzip tool:
================================================

- If there is a new version of the PclZip Modul, just copy the file
  "pclzip.lib.php" into the same directory like the file "pbzip.php".
  Then the new file will be used.

- Thanks to Vincent Blavet for the good PclZip library ! Hes website is
  at: http://www.phpconcept.net
